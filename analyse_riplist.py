#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Apr 29 21:39:08 2018

@author: till
"""

import pandas as pd
import time
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
import matplotlib.pyplot as plt
import matplotlib




class scrape_riplist():
    """ Scrape data from R.I.P. list """

    def __int__(self, url, page_end=24):
        """ initialise the URL, the webdriver and define the page to stop at """
        self.url = url
        self.driver = webdriver.Firefox()
        self.driver.get(self.url)
        self.list_of_entries = []
        self.page_end = page_end


    def scrape(self):
        """ Scrape the provided URL.
        FIXME: the amount of pages is hardcoded, I was too lazy to look into how to detect the last page in selenium.
        """
        self.page_number = 0

        while True:
            self.page_number += 1
            print("Page: ", self.page_number, " ...")

            try:
                table = self.driver.find_element_by_id('ripList')
                rows = table.find_elements_by_tag_name("tr")

                for row in rows:
                    cols = row.find_elements_by_tag_name("td")
                    entry = []
                    for col in cols:
                        entry.append(col.text)
                    if len(entry) != 0:
                        self.list_of_entries.append(entry)
            except:
                print("Could not find any elements ... ")
                pass

            print("done!")
            try:
                next_link = self.driver.find_element_by_id("ripList_next")
                next_link.click()
                time.sleep(5)
                WebDriverWait(self.driver, 10)
            except:
                print("Could not find any next link")
                break;

            if self.page_number >= self.page_end:
                print("Reached hard-coded end ... ")
                break;

        self.driver.close()


    def make_dataframe(self, columns):
        """ convert the scraped data into a DataFrame """
        self.df = pd.DataFrame(self.list_of_entries, columns=columns)


    def save_dataframe(self, save_name):
        """ save the DataFrame created from scraped data """
        self.df.to_pickle(save_name)



class analyse_riplist():
    """ Analyse the R.I.P. list """

    def __int__(self):
        """ nothing to do here """
        pass


    def load_dataframe(self, dataframe):
        """ load data into this class while it is still in memory """
        self.data = dataframe


    def load_pickle(self, filename):
        """ load data into this class from file """
        self.data = pd.read_pickle(filename)


    def replace_values(self, stringlist_to_replace, replacement):
        """ Replace a string or list of strings with another string """
        if type(stringlist_to_replace) is not list:
            stringlist_to_replace = [stringlist_to_replace]

        self.data = self.data.replace(stringlist_to_replace, replacement)


    def replace_content(self, key, content, replacement):
        """ replace CONTENT with REPLACEMENT for column KEY """
        self.data.loc[self.data[key].str.contains(content, case=False), key] = replacement


    def print_data(self, amount=10):
        """ debug function, prints the top x (default=10) entries of data """
        print(self.data.head(10))


    def get_dataset_sizes(self):
        """ creates a Series with countries and amount of data points for the respective country """
        return self.data['Country'].value_counts()


    def remove_rows_by_value(self, key, value):
        """ Removes all rows from dataframe with VALUE in column KEY"""
        self.data = self.data.drop(self.data.index[self.data[key] == value].tolist())


    def sort_data_by_key(self, key):
        """ sorts the dataframe by a KEY"""
        self.data = self.data.sort_values(by=[key])


    def plot_cause_of_death_count(self):
        """ plot a normalised overview of the global causes of death """
        self.CoD_statistic = self.data['Cause of Death'].value_counts(normalize=True)
        ax = self.CoD_statistic[ self.CoD_statistic > 7 ].plot.bar(rot=0)
        ax.set_xlabel("cause of death")
        ax.set_ylabel("count")
        ax.grid()


    def make_keydict(self, key):
        """ create a dictionary of DataFrames per KEY """
        uniques = self.data[key].unique()
        self.keydict = { element : pd.DataFrame for element in uniques }
        for newkey in self.keydict.keys():
            self.keydict[newkey] = self.data[:][self.data[key] == newkey]


    def remove_from_keydict(self, threshold=10):
        """ remove entries from keydict with less entries than THRESHOLD """
        keylist = list(self.keydict.keys())
        for key in keylist:
            if len(self.keydict[key]) < threshold:
                tmpdict = dict(self.keydict)
                del tmpdict[key]
                self.keydict = tmpdict


    def get_tops(self, key, amount=10):
        """ return a list of AMOUNT of indicies occuring most frequently in column KEY"""
        causelist = self.data[key].value_counts()[:amount]
        return list(causelist.index)


    def calculate_probability_of_cause(self, key):
        """ calculate the probability of KEY as cause of death depending on country """
        datasizes = self.get_dataset_sizes()
        self.make_keydict('Cause of Death')
        cause = self.keydict[key]['Country'].value_counts()
        self.probability = cause / datasizes
        self.probability.dropna()


    def calculate_probabilities_of_cause(self, keys):
        """ creates a list of series with the probability of a cause of death from KEYS list depending on country """
        cause_probs = []
        for cause in keys:
            self.calculate_probability_of_cause(cause)
            cause_probs.append(self.probability)
        self.cause_probabilities_country = pd.concat(cause_probs, axis=1)
        self.cause_probabilities_country.columns = keys


    def clear_probabilities_of_cause(self, threshold=30):
        """ removes countries from cause_probabilities_country that have less than THRESHOLD data points and replaces NaN with 0 """
        counts = self.get_dataset_sizes()
        self.cause_probabilities_country = self.cause_probabilities_country.drop(counts.index[counts < threshold])
        self.cause_probabilities_country = self.cause_probabilities_country.fillna(0)


    def plot_cause_of_death_by_country(self, country_key):
        """ plot a normalised overview of the 10 most common causes of death for the selected country """

        self.make_keydict('Country')

        self.stat = self.keydict[country_key]['Cause of Death'].value_counts(normalize=True)

        ax = self.stat[:10].plot.bar(rot=0)
        ax.set_xlabel("Causes of Death")
        ax.set_ylabel("probability")
        ax.set_title("Top 10 Causes of Death of musicians from " + country_key)
        #ax.set_yticklabels(range(0,100,2))
        ax.grid()
        
        
    def plot_cause_probabilities_by_country(self):
        """ plot causes of death grouped by country """
        matplotlib.style.use('seaborn')
        ax = self.cause_probabilities_country.plot(kind='bar',alpha=0.75, rot=0)
        ax.set_xlabel("Country")
        ax.set_ylabel("Probability")
        ax.set_title("Probability of different causes of death of metal musicians from different countries")
        xlabs = [item.get_text() for item in ax.get_xticklabels()]
        for i, label in enumerate(xlabs):
            if label == 'United Kingdom':
                xlabs[i] = 'UK'
            elif label == 'United States':
                xlabs[i] = 'US'
        ax.set_xticklabels(xlabs)




if __name__ == "__main__":

    url = "https://www.metal-archives.com/artist/rip"
    columns = ['Artist', 'Country', 'Bands', 'Date of Birth', 'Cause of Death']

    # scrape the website
    #S = scrape_riplist(url, 24)
    #S.scrape()
    #S.make_dataframe(columns)
    #S.save_dataframe("riplist.pkl")

    # anlyse data
    A = analyse_riplist()
    A.load_pickle("/home/till/Development/Python/riplist/riplist.pkl") # load from file

    # remove entries with Unknown or N/A as Cause of Death
    A.replace_values(['N/A', 'Unknown'], 'NAN')
    A.remove_rows_by_value('Cause of Death', 'NAN')

    # summarise some causes (pretty aggressive, but I don't have many data points anyway ... )
    A.replace_content('Cause of Death', 'cancer', 'Cancer')
    A.replace_content('Cause of Death', 'tumour', 'Cancer')
    A.replace_content('Cause of Death', 'tumor', 'Cancer')
    A.replace_content('Cause of Death', 'leukemia', 'Cancer')
    A.replace_content('Cause of Death', 'terror', 'Terrorism')
    A.replace_content('Cause of Death', 'injury', 'Injury')
    A.replace_content('Cause of Death', 'injuries', 'Injury')
    A.replace_content('Cause of Death', 'diabetis', 'Diabetes')
    A.replace_content('Cause of Death', 'diabetes', 'Diabetes')
    A.replace_content('Cause of Death', 'diabetic', 'Diabetes')
    A.replace_content('Cause of Death', 'fire', 'Accident')
    A.replace_content('Cause of Death', 'road', 'Traffic')
    A.replace_content('Cause of Death', 'crash', 'Traffic')
    A.replace_content('Cause of Death', 'bus', 'Traffic')
    A.replace_content('Cause of Death', 'car', 'Traffic')
    A.replace_content('Cause of Death', 'run over', 'Traffic')
    A.replace_content('Cause of Death', 'collision', 'Traffic')
    A.replace_content('Cause of Death', 'train', 'Traffic')
    A.replace_content('Cause of Death', 'surgery', 'Medicine')
    A.replace_content('Cause of Death', 'truck', 'Traffic')
    A.replace_content('Cause of Death', 'vehicle', 'Traffic')
    A.replace_content('Cause of Death', 'drunk driver', 'Traffic')
    A.replace_content('Cause of Death', 'hypothermia', 'Accident')
    A.replace_content('Cause of Death', 'avalance', 'Accident')
    A.replace_content('Cause of Death', 'fall', 'Accident')
    A.replace_content('Cause of Death', 'fell', 'Accident')
    A.replace_content('Cause of Death', 'elec', 'Accident')
    A.replace_content('Cause of Death', 'surgery', 'Medicine')
    A.replace_content('Cause of Death', 'complications', 'Medicine')
    A.replace_content('Cause of Death', 'falling', 'Accident')
    A.replace_content('Cause of Death', 'transplant', 'Medicine')
    A.replace_content('Cause of Death', 'accident', 'Accident')
    A.replace_content('Cause of Death', 'murder', 'Murdered')
    A.replace_content('Cause of Death', 'shot', 'Murdered')
    A.replace_content('Cause of Death', 'stab', 'Murdered')
    A.replace_content('Cause of Death', 'to death', 'Murdered')
    A.replace_content('Cause of Death', 'slaughter', 'Muredered')
    A.replace_content('Cause of Death', 'police', 'Murdered')
    A.replace_content('Cause of Death', 'gun', 'Murdered')
    A.replace_content('Cause of Death', 'shoot', 'Murdered')
    A.replace_content('Cause of Death', 'homicide', 'Muredered')
    A.replace_content('Cause of Death', 'suicide', 'Suicide')
    A.replace_content('Cause of Death', 'infection', 'Disease')
    A.replace_content('Cause of Death', 'fever', 'Disease')
    A.replace_content('Cause of Death', 'disease', 'Disease')
    A.replace_content('Cause of Death', 'heart', 'Disease')
    A.replace_content('Cause of Death', 'illness', 'Disease')
    A.replace_content('Cause of Death', 'epilepsy', 'Disease')
    A.replace_content('Cause of Death', 'asthma', 'Disease')
    A.replace_content('Cause of Death', 'sclerosis', 'Disease')
    A.replace_content('Cause of Death', 'pneu', 'Disease')
    A.replace_content('Cause of Death', 'cerebral', 'Disease')
    A.replace_content('Cause of Death', 'embolism', 'Disease')
    A.replace_content('Cause of Death', 'myeloma', 'Disease')
    A.replace_content('Cause of Death', 'sepsis', 'Disease')
    A.replace_content('Cause of Death', 'lymphoma', 'Disease')
    A.replace_content('Cause of Death', 'respiratory', 'Disease')
    A.replace_content('Cause of Death', 'stomach', 'Disease')
    A.replace_content('Cause of Death', 'typhus', 'Disease')
    A.replace_content('Cause of Death', 'problems', 'Disease')
    A.replace_content('Cause of Death', 'hypoxia', 'Disease')
    A.replace_content('Cause of Death', 'fetus', 'Disease')
    A.replace_content('Cause of Death', 'cyst', 'Disease')
    A.replace_content('Cause of Death', 'stroke', 'Stroke')
    A.replace_content('Cause of Death', 'seizure', 'Seizure')
    A.replace_content('Cause of Death', 'failure', 'Organ failure')
    A.replace_content('Cause of Death', 'organ', 'Organ failure')
    A.replace_content('Cause of Death', 'aneurism', 'Aneurysm')
    A.replace_content('Cause of Death', 'aneurysm', 'Aneurysm')
    A.replace_content('Cause of Death', 'alcohol', 'Drugs')
    A.replace_content('Cause of Death', 'cirrhosis', 'Drugs')
    A.replace_content('Cause of Death', 'drug', 'Drugs')
    A.replace_content('Cause of Death', 'overdose', 'Drugs')
    A.replace_content('Cause of Death', 'liver', 'Drugs')
    A.replace_content('Cause of Death', 'military', 'War')
    A.replace_content('Cause of Death', 'war', 'War')
    A.replace_content('Cause of Death', 'aids', 'AIDS')
    A.replace_content('Cause of Death', 'hiv', 'AIDS')
    A.replace_content('Cause of Death', 'natural', 'Natural')
    A.replace_content('Cause of Death', 'sleep', 'Natural')
    A.replace_content('Cause of Death', 'hurricane', 'Natural desaster')
    A.replace_content('Cause of Death', 'tsunami', 'Natural desaster')

    A.sort_data_by_key('Country')

    #A.plot_cause_of_death_count()
    #A.plot_cause_of_death_by_country(country_key='United States')

    # probability of the top X causes of death depending on country
    causes = A.get_tops('Cause of Death', amount=7)
    A.calculate_probabilities_of_cause(causes)
    A.clear_probabilities_of_cause(threshold=50)
    A.plot_cause_probabilities_by_country()

    # debug only (make it accessible so I can play with it in iPython)
    rawdata = A.data

    # print(A.cause_probabilities_country)
