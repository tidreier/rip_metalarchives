# R.I.P. Metal-Archives

This script provices tools to scrape the R.I.P. list from Metal-Archives and analyse it using pandas.

## Getting Started

Simply clone the repository and execute the script. The scraped data is included (from 2018-04-29) and can be loaded.

### Prerequisites

The script was written using Python 3.6.4 and requires the following packages:

```
pandas
selenium
matplotlib
```

### Installing

I wrote this on Linux (Manjaro) using the anaconda framework from Continuum Analytics and the Atom editor together with the hydrogen package. However, this script should run in every Python3 environment on every OS with the required packages installed.


## Data Analysis
Please note that the dataset is very limited and therefore **not** representative nor in any way statistically relevant.

![alternativetext](plots/probabilities_by_country_min50datapoints.png)

## License

This is open source. Do what ever you want.


 
